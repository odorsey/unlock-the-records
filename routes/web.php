<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Project;
use Illuminate\Support\Facades\Input;

Route::get('/', 'PagesController@index');
Route::get('search', 'SearchController@index');
Route::post('search', 'SearchController@search');
Route::get('about', 'PagesController@about');
Route::get('faqs', 'PagesController@faqs');
// Route::get('support', 'PagesController@support');
