## About Unlock the Records

Unlock the Records is a suggestion guide that allows users to identify the records and resources they need to help further their research on African American ancestors. The user enters a question or a phrase to indicate what piece of information he/she is looking for and then the interface offers a list of recommended records to pursue in order to accomplish research goals.
