// Setting underline for active navigated page
$(document).ready(function () {
    var url = window.location;
        $('.navbar-nav a').filter(function() {
            $(this).removeClass('active');
             return this.href == url;
        }).parent().addClass('active');
});

// Advanced Search Toggle
$(document).ready(function () {
  $('#adsearch').click(function (){
    $(".advanced").toggle();
  })
});

// Timed modal
$(document).ready(function(){
     setTimeout(function() {
       $('.call').modal('show');
     }, 120000); // Show after 2 min
     $('.support').click(function () {
       $('.call').modal('show');
     });
     // setTimeout(function() {
     //   $('#call').modal('hide');
     // }, 300000); // Hide after 5 min
});
