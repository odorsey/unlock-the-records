<nav class="navbar navbar-expand-md navbar-dark bg-transparent">
  <a class="navbar-brand" href="/">{{ config('app.name') }}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link active" href="/">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/about">About</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="/faqs">FAQs</a>
      </li> -->
      <li class="nav-item">
          <button type="button" class="nav-link btn support">Support</button>
      </li>
    </ul>
  </div>
</nav>
