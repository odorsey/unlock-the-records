<!-- <div class="container"> -->
<div class="row footer">
  <footer class="col-sm-12">
      <p class="text-center">
        <i class="fas fa-copyright"></i> 2019 All Rights Reserved.
        Made with love and pride.
      </p>
      <p class="text-center">
        <a href="https://www.patreon.com/oliviacodes" target="_blank"><i class="fab fa-patreon fa-lg footer-link"></i></a>
        <a href="https://twitter.com/oliviacodes" target="_blank"><i class="fab fa-twitter fa-lg footer-link"></i></a>
        <a href="mailto:contact@digitalblackhistory.com"><i class="fas fa-envelope fa-lg footer-link"></i></a>
      </p>
    </footer>
</div>
<!-- </div> -->
