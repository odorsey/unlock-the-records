<div class="modal fade call" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <!-- <h5 class="modal-title" id="exampleModalLabel">Email Results</h5> -->
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <h2>Like what you see?</h2>
      <h3>Contribute today.</h3>

      <p>It looks like you're enjoying "{{ config('app.name') }}"! 😏 If the site is helping you with your family history research, consider supporting the project by donating to my Patreon below:

      <p>Stay up-to-date on project updates and be the first to know about any new projects! With contributions from users like you, I can continue to create and maintain projects like "{{ config('app.name') }}."</p>

      <a href="https://www.patreon.com/bePatron?u=19529057" class="patreon" target="_blank"><img src="../../images/patreon.png" alt="Become a Patron" /></a>

      <h4>Not ready to contribute?</h4>
      <p>Share the project with your friends, family, and fellow researchers!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">No thanks</button>
      <a href="https://www.patreon.com/oliviacodes" target="_blank"><button type="button" class="btn btn-primary">Contribute</button></a>
    </div>
  </div>
</div>
</div>
