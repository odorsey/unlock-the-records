@extends('layouts.page')
@section('content')
<!-- <a href="/"><i class="fas fa-chevron-left"></i> Go back</a> -->
<div class="row search-question">
  <form action="/search" method="POST" role="search">
    {{ csrf_field() }}
    <div class="input-group input-group-lg">
        <input type="text" class="form-control" name="search" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="How do I find the birth date of my great-grandfather?">
        <div class="input-group-append">
              <button type="submit" class="btn btn-secondary" id = "submit">
              <i class="fas fa-search"></i>
        </div>
        </button>
    </div>
  </form>
</div>
<div class="row query">
    <h1 class="input">{{ $q }}</h1>
</div>

<!-- Modal -->
    @include('inc.call')

<!-- <div class="row">
    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-envelope"></i> Email yourself the results</button>
</div> -->
  <div class="results">
    <h2>Recommended Resources</h2>
    <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
      @if (count($results) > 0)
        <ul>
        @foreach($results as $result)
            <li>{{$result->source_name}}</li>
        @endforeach
        </ul>
      @else
        <p>No results found. Try entering a different question or phrase.</p>
        <p>Search for something you think should have a result? <a href="contact@digitalblackhistory.com">Send me an email</a> and I'll work on it!</p>
      @endif
  </div>
  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Email Results</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group input-group-lg">
            <input type="email" class="form-control" name="email" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="email@email.com">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send Email</button>
      </div>
    </div>
  </div>
</div>
@endsection
