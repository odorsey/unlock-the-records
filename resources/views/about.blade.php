@extends('layouts.page')
@section('content')
  <div class="about"
    <!-- Modal -->
        @include('inc.call')

    <h1>About "{{ config('app.name') }}"</h1>
    <p>"{{ config('app.name') }}" is an interactive guide that suggests resources to help further research on African American ancestors. Whether or not you can find important information about an ancestor can determine if you can fulfill a research goal. This project tries to make it easier for African Americans to find information on their ancestors.</p>

    <h2>Project Creation</h2>
    <p>I created this project because as a family historian myself, I was having a difficult time figuring out how to determine which record sets would provide me with the information I needed to research my own African American ancestors. As Black researchers, we already run into enough barriers trying to find our ancestors. My hope is that this project will help clear at least one of those obstacles for our community.</p>

    <p>If you have any questions or feedback about the site, <a href="contact@digitalblackhistory.com">send me an email</a> or reach out to me on Twitter at <a href="https://twitter.com/oliviacodes" target="_blank">@oliviacodes</a>.

    <h2>Disclaimer</h2>
    The original photograph used on this site belongs to <a href="https://unsplash.com/@k009034" target="_blank">Jukka Heinovirta</a> via Unsplash.
  </div>
@endsection
