@extends('layouts.app')
@section('content')
  <div id="intro">
    <h1 class="text-center">Find the records, find your family.</h1>
    <div class="question">
      <!-- <h1 class="text-center">{{ config('app.name') }}</h1> -->
      <div id="input-question">
        <form action="/search" method="POST" role="search">
          {{ csrf_field() }}
          <div class="input-group input-group-lg">
              <input type="text" class="form-control" name="search" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="How do I find the birth date of my great-grandfather?">
              <div class="input-group-append">
                    <button type="submit" class="btn btn-secondary" id = "submit">
                    <i class="fas fa-search"></i>
              </div>
              </button>
          </div>
          <!-- <button type="button" class="btn btn-secondary" id="adsearch">Advanced Search <i class="fas fa-chevron-down fa-lg"></i></button>
          <div class="advanced">
            <label>Year Range</label>
            <div class="row col-sm-8">
              <input type="text" class="form-control col-sm-3" name="year1" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="1960">

              <span class="space">to</span>

              <input type="text" class="form-control col-sm-3" name="year2" aria-label="Large" aria-describedby="inputGroup-sizing-sm" placeholder="1966">
            </div>

            <div class="row col-sm-8 state">
              <label>State</label>
              <select class="custom-select" id="inputGroupSelect01">
                <option selected>State</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
            </div>
        </div> -->
      </form>
    </div>
  </div>

    @include('inc.call')

    <div class="row arrow-circle">
      <a href="#section1"><i class="fas fa-chevron-down fa-3x arrow"></i></a>
    </div>
  </div>
</div>
  <div class="container">
    <div class="info-section" id="section1">
      <h2>Why Use "{{ config('app.name') }}"?</h2>
        <p><i class="fa fa-mouse-pointer fa-5x content-img"></i> As a beginning genealogist or family historian, the large amount of record sets available to you can be overwhelming. It can be difficult to figure out what record you need to help you make progress on your research. With so many record sets available, it's hard to know what you need to look for next!</p>

        <p>This website was created as a guide to help you figure out when you should use certain records. It will also help you find records that will confirm important pieces of information in your family tree. "{{ config('app.name') }}" is meant to serve as a starting place to either begin or refresh your research by offering some suggestions for moving forward with your research findings. Still, keep in mind that it will not tell you if your ancestor is in certain records.</p>
    </div>
    <div class="row arrow-circle">
      <a href="#section2"><i class="fas fa-chevron-down fa-3x arrow"></i></a>
    </div>
    <div class="info-section" id="section2">
      <h2>Crafting a Good Question</h2>
      <p><i class="fa fa-question fa-5x content-img"></i>In order to get valuable results from "{{ config('app.name') }}," make sure to search using a question or a phrase that clearly explains what you're looking for. The search is not case-sensitive, so feel free to use either lowercase or uppercase letters.</p>

      <p>For example, if you are looking for a record set that will identify the date of birth of an ancestor or verify military service, consider constructing a phrase like either one of the following:</p>

      <h3 class="snippet">Birth date of great aunt</h3>
      <h3 class="snippet">military service for grandfather</h3>
      <div class="row arrow-circle">
        <a href="#section3"><i class="fas fa-chevron-down fa-3x arrow"></i></a>
      </div>
    </div>
    <div class="info-section" id="section3">
      <h2>African American Genealogy Research</h2>
      <p><i class="fa fa-book-open fa-5x content-img"></i>
      There are several challenges that one will encounter when doing African American Genealogy Research. Due to slavery, it can be hard to find records that verify names, relationships, and other vital pieces of information. It takes creativity and persistence to overcome the infamous brickwall of 1870, where all African Americans finally appear in the United States Census as free people.</p>
      <p>However, it's important to know:</p>
      <h3>It is possible to perform family history research on African American ancestors.</h3>
      <p class="third">Despite these obstacles, there are many records that can help people find information on their African American ancestors. "{{ config('app.name') }}" aims to make it easier to identify these records and align them with your research goals.</p>
      
      <div class="row top"><a href="#"><i class="fas fa-chevron-up fa-3x arrow"></i></a></div>
      @include('inc.footer')
    </div>

@endsection
