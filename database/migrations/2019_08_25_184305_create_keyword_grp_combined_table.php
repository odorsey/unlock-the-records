<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeywordGrpCombinedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keyword_grp_combined', function (Blueprint $table) {
            $table->bigInteger('grp_id')->unsigned();
            $table->foreign('grp_id')->references('grp_id')->on('keyword_groups');
            $table->bigInteger('keyword_id')->unsigned();
            $table->foreign('keyword_id')->references('keyword_id')->on('keywords');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keyword_grp_combined');
    }
}
