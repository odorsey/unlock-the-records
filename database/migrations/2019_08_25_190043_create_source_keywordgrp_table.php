<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourceKeywordgrpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('source_keywordgrp', function (Blueprint $table) {
         $table->bigInteger('grp_id')->unsigned();
         $table->foreign('grp_id')->references('grp_id')->on('keyword_groups');
         $table->bigInteger('sourceType_id')->unsigned();
         $table->foreign('sourceType_id')->references('type_id')->on('source_type');
         $table->timestamps();
 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('source_keywordgrp');
    }
}
