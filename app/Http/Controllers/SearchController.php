<?php

namespace App\Http\Controllers;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use DB;

class SearchController extends Controller
{
    // Return the search screen
    public function index(Request $request)
    {
      return view('search.index');
    }

    // Run search query
    public function search(Request $request)
    {
    //  $q = Input::get('search');

      if($request->search)
      {
      $q = $request->search;
      $results = DB::table('source_type')
                   ->select('source_type.*')->distinct()
                   ->join('source_keywordgrp', 'source_keywordgrp.sourceType_id', '=', 'source_type.type_id')
                   ->join('keyword_grp_combined', 'keyword_grp_combined.grp_id', '=', 'source_keywordgrp.grp_id')
                   ->join('keywords', 'keywords.keyword_id', '=', 'keyword_grp_combined.keyword_id')
                   ->whereRaw("MATCH(keywords.name)AGAINST('$q')") // Note to self: needed to set keywords.name field to "fulltext" index. Match against gives better performance than "LIKE" and can expand to more columns.
                   ->orderBy('source_name', 'asc')
                   ->get();

      // this works
      $totalCount = DB::table('source_type')->count();

      // Always return the view with whatever comes from the DB
      return view('search.index', compact('results', 'q'));
    }
  }
}
